const locales = [
	{
		lang: 'en',
		personal_data: {
			heading: 'Personal data',
			trip_purpose: 'Trip purpose',
			trip_purpose_detailed: 'Tell more about trip purpose',
			registration_number: 'Registration number',
			trip_purpose_error_message: 'This field is required',
			registration_number_error_message: 'This field is required'
		},
		date_supply: {
			heading: 'Daily allowance',
			departure_date: 'Departure Date',
			departure_date_error_message: 'This field is required',
			arrival_date: 'Arrival Date',
			arrival_date_error_message: 'This field is required',
			departure_time: 'Departure time',
			departure_time_error_message: {
				empty_value: 'This field is required',
				invalid_inconsistency_value: 'The trip begins after it ends',
				invalid_format_value: 'Invalid date'
			},
			arrival_time: 'Arrival time',
			arrival_time_error_message: {
				empty_value: 'This field is required',
				invalid_inconsistency_value: 'The trip ends before it begins',
				invalid_format_value: 'Invalid date'
			},
			date_placeholder: 'dd.mm.yyyy',
			time_placeholder: 'hh.mm',
			week: [
				{
					long: 'Monday',
					short: 'Mon'
				},
				{
					long: 'Tuesday',
					short: 'Tue'
				},
				{
					long: 'Wednesday',
					short: 'Wed'
				},
				{
					long: 'Thursday',
					short: 'Thu'
				},
				{
					long: 'Friday',
					short: 'Fri'
				},
				{
					long: 'Saturday',
					short: 'Sat'
				},
				{
					long: 'Sunday',
					short: 'Sun'
				},
			],
			months: [
				{
					long: 'January',
					short: 'Jan'
				},
				{
					long: 'February',
					short: 'Feb'
				},
				{
					long: 'March',
					short: 'Mar'
				},
				{
					long: 'April',
					short: 'Apr'
				},
				{
					long: 'May',
					short: 'May'
				},
				{
					long: 'June',
					short: 'Jun'
				},
				{
					long: 'July',
					short: 'Jul'
				},
				{
					long: 'August',
					short: 'Aug'
				},
				{
					long: 'September',
					short: 'Sep'
				},
				{
					long: 'October',
					short: 'Oct'
				},
				{
					long: 'November',
					short: 'Nov'
				},
				{
					long: 'December',
					short: 'Dec'
				}
			],
			total_label: 'Total daily allowances:',
			free_meals: 'Free meals:',
			meal_allowances: 'Full daily allowance:',
			half_meal_allowances: 'A partial per diem:',
			meal_price: 'Allowance unit price:',
			meal_total: 'Total',
			meal_hours: 'Total hours:',
			quantity_placeholder: 'qty'
		},
		mileage: {
			heading: 'Mileage allowances',
			distance: 'km',
			departure_address: 'Departure address',
			destination_address: 'Destination address',
			passengers: 'Passengers',
			kilometers: 'km',
			mileage_error_message: 'This field is required',
			total_mileage_km: 'Total mileage:',
			total_mileage_allowance: 'Total mileage allowance:'
		},
		total: {
			total_paid: 'Total paid:'
		}
	},

	{
		lang: 'fi',
		personal_data: {
			heading: 'Henkilökohtaiset tiedot',
			trip_purpose: 'Tarkoitus',
			trip_purpose_detailed: 'Lisätietoja matkasta',
			registration_number: 'Rekisterinumero',
			trip_purpose_error_message: 'Tämä kenttä pitää täyttää',
			registration_number_error_message: 'Tämä kenttä pitää täyttää'
		},
		date_supply: {
			heading: 'Päivärahat',
			departure_date: 'Lähtö pvm',
			departure_date_error_message: 'Tämä kenttä pitää täyttää',
			arrival_date: 'Saapumispäivä',
			arrival_date_error_message: 'Tämä kenttä pitää täyttää',
			departure_time: 'Lähtö klo',
			departure_time_error_message: {
				empty_value: 'Tämä kenttä pitää täyttää',
				invalid_inconsistency_value: 'Matka alkaa sen päätyttyä.',
				invalid_format_value: 'Virheellinen päivämäärä'
			},
			arrival_time: 'Loppu klo',
			arrival_time_error_message: {
				empty_value: 'Tämä kenttä pitää täyttää',
				invalid_inconsistency_value: 'Matka loppuu ennen kuin se alkaa',
				invalid_format_value: 'Virheellinen päivämäärä'
			},
			date_placeholder: 'pp.kk.vvvv',
			time_placeholder: 'tt.mm',
			week: [
				{
					long: 'Maanantai',
					short: 'Ma'
				},
				{
					long: 'Tiistai',
					short: 'Ti'
				},
				{
					long: 'Keskiviikko',
					short: 'Ke'
				},
				{
					long: 'Torstai',
					short: 'To'
				},
				{
					long: 'Perjantai',
					short: 'Pe'
				},
				{
					long: 'Lauantai',
					short: 'La'
				},
				{
					long: 'Sunnuntai',
					short: 'Su'
				},
			],
			months: [
				{
					long: 'tammikuu',
					short: 'tam'
				},
				{
					long: 'helmikuu',
					short: 'hel'
				},
				{
					long: 'maaliskuu',
					short: 'maa'
				},
				{
					long: 'huhtikuu',
					short: 'huh'
				},
				{
					long: 'toukokuu',
					short: 'tou'
				},
				{
					long: 'kesäkuu',
					short: 'kes'
				},
				{
					long: 'heinäkuu',
					short: 'hei'
				},
				{
					long: 'elokuu',
					short: 'elo'
				},
				{
					long: 'syyskuu',
					short: 'syy'
				},
				{
					long: 'lokakuu',
					short: 'lok'
				},
				{
					long: 'marraskuu',
					short: 'mar'
				},
				{
					long: 'joulukuu',
					short: 'jou'
				}
			],
			total_label: 'Päivärahat yhteensä:',
			free_meals: 'Ilmaiset ateriat:',
			meal_allowances: 'Kokopäiväraha:',
			half_meal_allowances: 'Osapäiväraha:',
			meal_price: 'Kokopäiväraha hinta:',
			meal_total: 'Kaikki yhteensä',
			meal_hours: 'Tunteja yhteensä:',
			quantity_placeholder: 'Kpl'
		},
		mileage: {
			heading: 'Kilometrikorvaukset',
			distance: 'km',
			departure_address: 'Lähtöosoite',
			destination_address: 'Kohdeosoite',
			passengers: 'Lisämatkustajat',
			kilometers: 'km',
			mileage_error_message: 'Tämä kenttä pitää täyttää',
			total_mileage_km: 'Kokonaisnäyttö:',
			total_mileage_allowance: 'Kilometrikorvaus yhteensä:'
		},
		total: {
			total_paid: 'Maksetaan yhteensä:'
		}
	}
]

export default locales