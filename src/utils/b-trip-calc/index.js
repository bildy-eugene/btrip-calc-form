import BTripCalc from './BTripCalc.vue'

export default {
	install(Vue) {
		Vue.component('b-trip-calc', BTripCalc)
	}
}