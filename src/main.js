import Vue from 'vue'
import App from './App.vue'
import store from './store'
import BTripCalc from './utils/b-trip-calc'

Vue.use(BTripCalc)

Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
